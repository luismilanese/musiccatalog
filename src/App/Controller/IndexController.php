<?php

namespace MusicCatalog\App\Controller;

use Aura\Sql\ExtendedPdo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    /**
     * @Route("/")
     */
    public function index()
    {
//        $doctrine = $this->getDoctrine()->getConnection();
//
//        $result = $doctrine->fetchAll("SELECT * FROM teste");
//
//        var_dump($result); exit;

        /** @var ExtendedPdo $extendedPDO */
        $extendedPDO = $this->get('ExtendedPDO');
        $r = $extendedPDO->fetchAll('SELECT * FROM teste');

        var_dump($r);

        return $this->render('index.html.mustache');
    }
}