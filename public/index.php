<?php

require __DIR__ . '/../app/autoload.php';
require __DIR__ . '/../app/kernel.php';

$kernel = new AppKernel('dev', true);
$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);